# GOLANG

Vous devez développer un plugin Telegraf qui enregistre des valeurs données par powertop (https://wiki.archlinux.org/index.php/powertop) sur l’infrastructure docker hébergeant le blog (cf partie Docker) dans une DB InfluxDB. Afin d’y suivre les graphes de consommation électrique des machines.(https://www.influxdata.com/training/write-your-own-telegraf-plugin/)

Votre projet Golang sera testé et déployé en image Docker via GITLAB CI

# DOCKER

En plus du projet Golang, il vous sera demandés d’installer une infrastructure Web hébergeant un Blog (libre choix de la solution, mais obligation d’une BDD déportée). Tous les services devront être containérisés dans leur propre environnement en respectant :

•	Un LoadBalancing est obligatoire 
•	Vous devez créer vos propres images Docker via dockerfile (toutes récupérations d’image existante est éliminatoire) 
•	Les images seront rajoutées à un DOCKER REGISTRY PRIVÉE. 
•	Le Docker registry devra également gérer l’authentification et les contrôles d’accès des utilisateurs via un « authentification server ». Vous aurez le choix de référencer vos utilisateurs via des ACL, ou MongoDB, ou LDAP, ou un fichier YAML (partie supplémentaire pour les quadrinômes)
Votre projet Blog sera testé et déployé en image Docker via GITLAB CI

# CI/CD

Vous utiliserez Gitlab-CI pour automatiser toutes les taches de déploiement mais également les tests du bon fonctionnement des applications.

Nous vous proposons un découpage en trois stages : 

•	Build, 
•	Test 
•	Deploy.
La partie « Test » vérifiera que vos projets GO et BLOG sont fonctionnels. 

Le déploiement se fera sur 3 containers en load Balancing avec l’outils de votre choix. (Pour les quadrinômes, il se fera sur 3 containers en load Balancing et sur 3 autres containers en Failover )